package ru.alexandrov.controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.alexandrov.entity.Entity;

@RestController
@RequestMapping("/hello")
public class Controller {
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public Entity getEntity(ModelMap model){
        return createMockEntity();
    }

    private Entity createMockEntity() {
        Entity entity = new Entity();
        entity.setId(1);
        entity.setName("Entity");
        return entity;
    }
}
